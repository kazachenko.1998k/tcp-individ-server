#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <pthread.h>
#include <string>
#include <list>
#include <thread>
#include <iostream>
#include <c++/8/sstream>
#include <cstring>
#include <netinet/in.h>
#include <arpa/inet.h>
#include "sql.cpp"

using namespace std;

int listener;
pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;
list<string> threadsID;
list<int> socks;
list<thread> threads;


string toStr(thread::id id) {
    stringstream ss;
    ss << id;
    return ss.str();
}

string listToString() {
    string listThreads = "list threadsID: ";
    for (const string &v : threadsID) {
        listThreads += v;
        listThreads += " ";
    }
    listThreads += "\n";
    for (thread &v : threads) {
        listThreads += toStr(v.get_id());
        listThreads += " ";
    }
    listThreads += "\n";

    return listThreads;
}

list<string> split(const string &s, char delimiter) {
    list<string> tokens;
    string token;
    istringstream tokenStream(s);
    while (getline(tokenStream, token, delimiter)) {
        tokens.push_back(token);
    }
    return tokens;
}

void exit() {
    shutdown(listener, 2);
    close(listener);
}

bool containsThread(const string &id) {
    for (const string &elem : threadsID) {
        if (strcmp(elem.c_str(), id.c_str()) == 0) {
            return true;
        }
    }
    return false;
}

void kill(const string &id, string &pString) {
    if (containsThread(id)) {
        threadsID.remove(id);
        pString = "killed ";
        pString += id;
    } else {
        pString = "Can not kill this";
    }
    pString += "\n";
}

void commandLine() {
    while (true) {
        string message;
        getline(cin, message);
        list<string> listSplit = split(message, ' ');
        if (strcmp(message.c_str(), string("list").c_str()) == 0) {
            cout << listToString();
        } else if (strcmp(message.c_str(), string("exit").c_str()) == 0) {
            cout << "Exit";
            exit();
            break;
        } else {
            listSplit = split(message, ' ');
            if (strcmp(listSplit.front().c_str(), string("kill").c_str()) == 0) {
                listSplit.pop_front();
                kill(listSplit.front(), message);
                cout << message;
            }
        }
    }
}

string listCommand() {
    string message;
    message += "exit - выход\n";
    message += "signUp (ваш логин) XXXX (роль) [CLIENT, PERFORMER] - создать нового пользователя\n";
    message += "signIn (ваш логин) XXXX - вход\n";
    message += "listMy - список ваших проектов для выполнения\n";
    message += "listAll - список всех проектов\n";
    message += "listOpen - список всех открытых проектов\n";
    message += "update (ID заказа) INT (состояние) [OPEN, CLOSE, DOING] - обновить зсостояние проекта\n";
    message += "insert (название проекта) \"ХХХХ\" (описание) \"ХХХХ\" (цена) INT- добавить новый проекта\n";
    return message;
}

void fun(int sock) {
    socklen_t len;
    struct sockaddr_storage addr{};
    char ipstr[INET6_ADDRSTRLEN];
    int port;
    len = sizeof addr;
    getpeername(sock, (struct sockaddr *) &addr, &len);
    auto *s = (struct sockaddr_in *) &addr;
    port = ntohs(s->sin_port);
    inet_ntop(AF_INET, &s->sin_addr, ipstr, sizeof ipstr);
    string hello = "Peer IP address: ";
    hello += ipstr;
    hello += ":";
    hello += to_string(port);
    hello += "\n";
    hello += listCommand();
    send(sock, hello.c_str(), 1024, 0);

    string name;
    string role;

    while (true) {
        string message;
        char buf[1024];
        int bytes_read;

        if (!containsThread(toStr(this_thread::get_id()))) {
            break;
        }
        bytes_read = read(sock, buf, 1024);
        if (bytes_read <= 0) {
            break;
        }
        if (!containsThread(toStr(this_thread::get_id()))) {
            break;
        }
        cout << buf << "\n";
        if (strcmp(buf, string("help").c_str()) == 0) {
            message = listCommand();
        } else if (strcmp(buf, string("exit").c_str()) == 0) {
            message = "Good bye my friend";
        } else if (strcmp(buf, string("listMy").c_str()) == 0) {
            if (strcmp(role.c_str(), string("CLIENT").c_str()) == 0) {
                message = selectMyTasksClient(name);
            } else {
                message = selectMyTasks(name);
            }
        } else if (strcmp(buf, string("listAll").c_str()) == 0) {
            message = selectAllTasks();
        } else if (strcmp(buf, string("listOpen").c_str()) == 0) {
            message = selectOpenTasks();
        } else {
            list<string> listSplit = split(buf, ' ');
            string com = listSplit.front();
            if (strcmp(com.c_str(), string("signUp").c_str()) == 0) {
                if (listSplit.size() < 3) {
                    message = listCommand();
                } else {
                    listSplit.pop_front();
                    name = listSplit.front();
                    listSplit.pop_front();
                    role = listSplit.front();
                    message = insertUser(name, role);
                }
            } else if (strcmp(com.c_str(), string("signIn").c_str()) == 0) {
                if (listSplit.size() < 2) {
                    message = listCommand();
                } else {
                    listSplit.pop_front();
                    name = listSplit.front();
                    role = selectRoleByName(name);
                    if (role.length() > 0) {
                        message = "Hello ";
                        message += name;
                        message += " you are ";
                        message += role;
                    } else {
                        message = "Sorry, but user with name ";
                        message += name;
                        message += " could'n find...";
                    }
                }
            } else if (role.length() == 0) {
                message = "You are not signIn";
            } else if (strcmp(com.c_str(), string("update").c_str()) == 0) {
                if (listSplit.size() < 2) {
                    message = listCommand();
                } else {
                    listSplit.pop_front();
                    string id = listSplit.front();
                    listSplit.pop_front();
                    string state = listSplit.front();
                    if (strcmp(role.c_str(), string("CLIENT").c_str()) == 0) {
                        message = updateTasks(id, "DOING", name);
                    } else if (strcmp(role.c_str(), string("PERFORMER").c_str()) == 0) {
                        if (listSplit.size() < 2) {
                            message = listCommand();
                        } else {
                            if (strcmp(state.c_str(), string("OPEN").c_str()) == 0) {
                                message = updateTasks(id, state, "null");
                            } else {
                                message = updateTasks(id, state);
                            }
                        }
                    } else {
                        message = "Sorry, but we can not do it";
                    }
                }
            } else if (strcmp(listSplit.front().c_str(), string("insert").c_str()) == 0) {
                if (strcmp(role.c_str(), string("CLIENT").c_str()) == 0) {
                    message = "Sorry, but we can not add task";
                } else {
                    list<string> listSplitText = split(buf, '\"');
                    if (listSplitText.size() < 5) {
                        message = listCommand();
                    } else {
                        listSplitText.pop_front();
                        string nameT = listSplitText.front();
                        listSplitText.pop_front();
                        listSplitText.pop_front();
                        string descT = listSplitText.front();
                        listSplitText.pop_front();
                        string priceT = listSplitText.front();
                        message = insertTasks(nameT, descT, priceT, "OPEN", name, "null");
                    }
                }
            } else {
                message = listCommand();
            }
        }
        if (!containsThread(toStr(this_thread::get_id()))) {
            break;
        }
        send(sock, message.c_str(), 1024, 0);
        if (!containsThread(toStr(this_thread::get_id()))) {
            break;
        }
        if (strcmp(buf, string("exit").c_str()) == 0) {
            break;
        }
    }
    shutdown(sock, 2);
    close(sock);
    pthread_mutex_lock(&mutex);
    threadsID.remove(toStr(this_thread::get_id()));
    socks.remove(sock);
    pthread_mutex_unlock(&mutex);
}

int main(int argc, char *argv[]) {
    connect();
    int sock;
    struct sockaddr_in addr{};
    listener = socket(AF_INET, SOCK_STREAM, 0);
    if (listener < 0) {
        perror("socket");
        exit(1);
    }

    addr.sin_family = AF_INET;
    addr.sin_port = htons(atoi(argv[1]));
    addr.sin_addr.s_addr = INADDR_ANY;
    const int on = 1;
    setsockopt(listener, SOL_SOCKET, SO_REUSEADDR, &on, sizeof on);
    if (bind(listener, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("bind");
        exit(2);
    }

    listen(listener, 1);
    thread cl(commandLine);
    while (true) {
        sock = accept(listener, nullptr, nullptr);
        if (sock < 0) {
            perror("accept");
            break;
        }
        pthread_mutex_lock(&mutex);
        threads.emplace_back([&] { fun(sock); });
        socks.emplace_back(sock);
        threadsID.emplace_back(toStr(threads.back().get_id()));
        pthread_mutex_unlock(&mutex);
    }

    cl.join();

    pthread_mutex_lock(&mutex);
    for (int &socked : socks) {
        shutdown(socked, 2);
        close(socked);
    }
    pthread_mutex_unlock(&mutex);

    for (thread &thread : threads) {
        if (containsThread(toStr(thread.get_id()))) {
            thread.join();
        }
    }

    PQfinish(conn);
    return 0;

}