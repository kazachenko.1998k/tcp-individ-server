//
// Created by konstantin on 15.11.2019.
//
#include <cstdlib>
#include <libpq-fe.h>

using namespace std;

#define UNUSED(x) (void)(x)


static PGconn *conn = nullptr;
static PGresult *res = nullptr;

static string terminate(int code) {
    string result = string("allOk");
    if (code != 0) {
        fprintf(stderr, "%s\n", PQerrorMessage(conn));
        result = string(PQerrorMessage(conn));
    }
    if (res != nullptr)
        PQclear(res);

//    if (conn != nullptr)
//        PQfinish(conn);
    return result;
}

static void clearRes() {
    PQclear(res);
    res = nullptr;
}

static void processNotice(void *arg, const char *message) {
    UNUSED(arg);
    UNUSED(message);

    // do nothing
}

int dropAllTable() {

    res = PQexec(conn, "DROP TABLE IF EXISTS tasks;");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (1);
    }

    res = PQexec(conn, "DROP TABLE IF EXISTS users;");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (1);
    }

    res = PQexec(conn, "DROP TYPE IF EXISTS state_order;");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (1);
    }

    res = PQexec(conn, "DROP TYPE IF EXISTS type_user;");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (1);
    }

    return 0;
}

bool connect() {
    int libpq_ver = PQlibVersion();
    printf("Version of libpq: %d\n", libpq_ver);

    conn = PQconnectdb("user=someuser password=qwe host=127.0.0.1 dbname=dbfree");
    if (PQstatus(conn) != CONNECTION_OK)
        return (false);

    PQsetNoticeProcessor(conn, processNotice, nullptr);

    int server_ver = PQserverVersion(conn);
    char *user = PQuser(conn);
    char *db_name = PQdb(conn);
    printf("Server version: %i\n", server_ver);
    printf("User: %s\n", user);
    printf("Database name: %s\n", db_name);

//    dropAllTable();

    res = PQexec(conn, "CREATE TYPE type_user AS ENUM ("
                       "'PERFORMER',"
                       "'CLIENT');");
    clearRes();

    res = PQexec(conn, "CREATE TYPE state_order AS ENUM ("
                       "'OPEN',"
                       "'DOING',"
                       "'CLOSE');");
    clearRes();

    res = PQexec(conn, "CREATE TABLE IF NOT EXISTS users "
                       "(id SERIAL PRIMARY KEY, name VARCHAR(64) UNIQUE, "
                       "role type_user);");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (false);
    }
    clearRes();

    res = PQexec(conn,
                 "CREATE TABLE IF NOT EXISTS tasks "
                 "(id SERIAL PRIMARY KEY, name VARCHAR(128), "
                 "description VARCHAR, price INT, "
                 "state state_order NOT NULL,"
                 "id_user_create INT NOT NULL,"
                 "id_user_doing INT,"
                 "FOREIGN KEY (id_user_create) REFERENCES users(id),"
                 "FOREIGN KEY (id_user_doing) REFERENCES users(id),"
                 "CHECK ((id_user_doing IS NULL AND state = 'OPEN') OR (id_user_doing IS NOT NULL AND state <> 'OPEN')));");
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        terminate(1);
        return (false);
    }
    clearRes();

    return true;
}

string selectAllUsers() {
    string result;
    res = PQexec(conn, "SELECT * "
                       "FROM users;");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);

    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        char *id = PQgetvalue(res, i, 0);
        char *name = PQgetvalue(res, i, 1);
        char *role = PQgetvalue(res, i, 2);
        result += "Id: ";
        result += id;
        result += " Name: ";
        result += name;
        result += " Role: ";
        result += role;
        result += "\n";
    }

    result += "Total: " + to_string(nrows) + " rows\n";
    clearRes();
    return result;
}

string selectAllTasks() {
    string result;
    res = PQexec(conn, "SELECT * "
                       "FROM tasks;");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);


    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        char *id = PQgetvalue(res, i, 0);
        char *name = PQgetvalue(res, i, 1);
        char *description = PQgetvalue(res, i, 2);
        char *price = PQgetvalue(res, i, 3);
        char *state = PQgetvalue(res, i, 4);
        char *id_user_create = PQgetvalue(res, i, 5);
        char *id_user_doing = PQgetvalue(res, i, 6);
        result += "Id: ";
        result += id;
        result += " Name: ";
        result += name;
        result += " Description: ";
        result += description;
        result += " Price: ";
        result += price;
        result += " State: ";
        result += state;
        result += " id_user_create: ";
        result += id_user_create;
        result += " id_user_doing: ";
        result += id_user_doing;
        result += "\n";
    }

    result += "Total: " + to_string(nrows) + " rows\n";
    clearRes();
    return result;
}

string selectMyTasks(const string &myName) {
    string result;
    const char *query =
            "SELECT * FROM tasks JOIN users on id_user_create = users.id LEFT JOIN users AS dev on id_user_doing = dev.id WHERE users.name = $1;";
    const char *params[1];

    params[0] = myName.c_str();
    res = PQexecParams(conn, query, 1, nullptr, params,
                       nullptr, nullptr, 0);

    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);

    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        char *id = PQgetvalue(res, i, 0);
        char *name = PQgetvalue(res, i, 1);
        char *description = PQgetvalue(res, i, 2);
        char *price = PQgetvalue(res, i, 3);
        char *state = PQgetvalue(res, i, 4);
        char *id_user_doing = PQgetvalue(res, i, 6);
        char *name_user_doing = PQgetvalue(res, i, 11);
        result += "Id: ";
        result += id;
        result += " Name: ";
        result += name;
        result += " Description: ";
        result += description;
        result += " Price: ";
        result += price;
        result += " State: ";
        result += state;
        result += " id_user_doing: ";
        result += id_user_doing;
        result += " Name user doing: ";
        result += name_user_doing;
        result += "\n";
    }

    result += "Total: " + to_string(nrows) + " rows\n";
    clearRes();
    return result;
}

string selectMyTasksClient(const string &myName) {
    string result;
    const char *query =
            "SELECT * FROM tasks JOIN users on id_user_doing = users.id LEFT JOIN users AS dev on id_user_create = dev.id WHERE users.name = $1;";
    const char *params[1];

    params[0] = myName.c_str();
    res = PQexecParams(conn, query, 1, nullptr, params,
                       nullptr, nullptr, 0);

    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);

    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        char *id = PQgetvalue(res, i, 0);
        char *name = PQgetvalue(res, i, 1);
        char *description = PQgetvalue(res, i, 2);
        char *price = PQgetvalue(res, i, 3);
        char *state = PQgetvalue(res, i, 4);
        char *id_user_created = PQgetvalue(res, i, 5);
        char *name_user_doing = PQgetvalue(res, i, 11);
        result += "Id: ";
        result += id;
        result += " Name: ";
        result += name;
        result += " Description: ";
        result += description;
        result += " Price: ";
        result += price;
        result += " State: ";
        result += state;
        result += " Id user created: ";
        result += id_user_created;
        result += " Name user created: ";
        result += name_user_doing;
        result += "\n";
    }

    result += "Total: " + to_string(nrows) + " rows\n";
    clearRes();
    return result;
}

string selectOpenTasks() {
    string result;
    res = PQexec(conn, "SELECT * "
                       "FROM tasks WHERE state = 'OPEN';");
    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);

    if (PQresultStatus(res) != PGRES_TUPLES_OK)
        return terminate(1);

    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        char *id = PQgetvalue(res, i, 0);
        char *name = PQgetvalue(res, i, 1);
        char *description = PQgetvalue(res, i, 2);
        char *price = PQgetvalue(res, i, 3);
        result += "Id: ";
        result += id;
        result += " Name: ";
        result += name;
        result += " Description: ";
        result += description;
        result += " Price: ";
        result += price;
        result += "\n";
    }

    result += "Total: " + to_string(nrows) + " rows\n";
    clearRes();
    return result;
}

string selectRoleByName(const string &myName) {
    string result;
    const char *query =
            "SELECT role FROM users WHERE name = $1;";
    const char *params[1];

    params[0] = myName.c_str();
    res = PQexecParams(conn, query, 1, nullptr, params,
                       nullptr, nullptr, 0);

    if (PQresultStatus(res) != PGRES_TUPLES_OK) {
        return terminate(1);
    }
    int nrows = PQntuples(res);
    for (int i = 0; i < nrows; i++) {
        char *role = PQgetvalue(res, i, 0);
        result += role;
    }
    clearRes();
    return result;
}

string insertUser(const string &name, const string &role) {
    const char *query =
            "INSERT INTO users (name, role) "
            " VALUES ($1, $2);";
    const char *params[2];

    params[0] = name.c_str();
    params[1] = role.c_str();
    res = PQexecParams(conn, query, 2, nullptr, params,
                       nullptr, nullptr, 0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        return terminate(1);
    }
    clearRes();
    return "true";
}

string insertTasks(const string &name,
                   const string &description,
                   const string &price,
                   const string &state,
                   const string &id_user_create,
                   const string &id_user_doing) {
    const char *query =
            "INSERT INTO tasks (name, description, price, state, id_user_create, id_user_doing) "
            " VALUES ($1, $2, $3, $4, (SELECT id FROM users WHERE name = $5), NULLIF($6,'null')::integer);";
    const char *params[6];

    params[0] = name.c_str();
    params[1] = description.c_str();
    params[2] = price.c_str();
    params[3] = state.c_str();
    params[4] = id_user_create.c_str();
    params[5] = id_user_doing.c_str();
    res = PQexecParams(conn, query, 6, nullptr, params,
                       nullptr, nullptr, 0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        return terminate(1);
    }
    clearRes();
    return "true";
}

string updateTasks(const string &id,
                   const string &state,
                   const string &id_user_doing) {
    const char *query =
            "UPDATE tasks set state = $2, id_user_doing = (SELECT id FROM users WHERE name = $3) WHERE id = $1;";
    const char *params[3];

    params[0] = id.c_str();
    params[1] = state.c_str();
    params[2] = id_user_doing.c_str();
    res = PQexecParams(conn, query, 3, nullptr, params,
                       nullptr, nullptr, 0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        return terminate(1);
    }
    clearRes();
    return "true";
}

string updateTasks(const string &id,
                   const string &state) {
    const char *query =
            "UPDATE tasks set state = $2 WHERE id = $1;";
    const char *params[2];

    params[0] = id.c_str();
    params[1] = state.c_str();
    res = PQexecParams(conn, query, 2, nullptr, params,
                       nullptr, nullptr, 0);
    if (PQresultStatus(res) != PGRES_COMMAND_OK) {
        return terminate(1);
    }
    clearRes();
    return "true";
}

int testtt() {
    if (!connect()) return 1;

    printf("%s\n", insertUser("Kostya", "PERFORMER").c_str());
    printf("%s\n", insertUser("Kostya2", "CLIENT").c_str());
    printf("%s\n", selectRoleByName("Kostya22").c_str());
    printf("%s\n", insertTasks("prosto rabota", "zdes' mozet byt' chto угодно", "123", "OPEN", "1",
                               "null").c_str());
    printf("%s\n", selectMyTasks("Kostya").c_str());
    printf("%s\n", selectAllTasks().c_str());
    printf("%s\n", updateTasks("1", "CLOSE", "1").c_str());
    printf("%s\n", selectMyTasks("Kostya").c_str());
    printf("%s\n", selectAllTasks().c_str());
    printf("%s\n", selectAllUsers().c_str());
    PQfinish(conn);
    return 0;
}
